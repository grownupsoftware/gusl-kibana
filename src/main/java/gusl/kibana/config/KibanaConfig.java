package gusl.kibana.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.SecretDeserializer;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 06/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class KibanaConfig {

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String host;
    @JsonDeserialize(using = SecretDeserializer.class)
    private String username;
    @JsonDeserialize(using = SecretDeserializer.class)
    @ToStringMask
    private String password;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
