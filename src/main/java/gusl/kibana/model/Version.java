package gusl.kibana.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Version {

    private String number;
    @JsonProperty(value = "build_hash")
    private String buildHash;
    @JsonProperty(value = "build_number")
    private String buildNumber;
    @JsonProperty(value = "build_snapshot")
    private Boolean buildSnapshot;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
