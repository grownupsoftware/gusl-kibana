package gusl.kibana.model;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Status {

    private Overall overall;
    @ToStringCount
    private List<PluginStatus> statuses;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
