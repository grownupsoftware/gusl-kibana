package gusl.kibana.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class KibanaStats {

    private String name;
    private String uuid;
    private Version version;
    private Status status;
    private Metrics metrics;

    @JsonIgnore
    public boolean isUp() {
        if (status != null) {
            if (status.getOverall() != null) {
                return status.getOverall().isUp();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
