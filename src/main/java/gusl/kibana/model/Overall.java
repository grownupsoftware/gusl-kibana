package gusl.kibana.model;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Overall {

    private static final String OK = "green";

    private LocalDateTime since;
    private String state;
    private String title;
    private String nickname;
    private String icon;
    private String uiColor;

    public boolean isUp() {
        return OK.equals(state);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
