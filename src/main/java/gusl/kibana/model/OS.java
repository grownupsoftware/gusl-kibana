package gusl.kibana.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class OS {

    private String platform;
    private String platformRelease;
    @JsonProperty(value = "uptime_in_millis")
    private Long uptimeInMillis;
    private String distro;
    private String distroRelease;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
