package gusl.kibana.model;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class PluginStatus {
    private String id;
    private String message;
    private LocalDateTime since;
    private String state;
    private String icon;
    private String uiColor;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
