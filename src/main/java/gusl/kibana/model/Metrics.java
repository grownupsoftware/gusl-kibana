package gusl.kibana.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 27/10/2021
 */
@Getter
@Setter
@NoArgsConstructor
public class Metrics {

    @JsonProperty(value = "last_updated")
    private LocalDateTime lastUpdated;
    @JsonProperty(value = "collection_interval_in_millis")
    private Integer collectionIntervalInMillis;
    @JsonProperty(value = "os")
    private OS os;
    @JsonProperty(value = "response_times")
    private ResponseTimes responseTimes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
