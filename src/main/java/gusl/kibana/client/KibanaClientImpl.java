package gusl.kibana.client;

import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.Utils;
import gusl.elastic.kibana.IndexPattern;
import gusl.elastic.utils.ElasticConstants;
import gusl.kibana.config.KibanaConfig;
import gusl.kibana.model.KibanaStats;
import gusl.node.jersey.JerseyHttpConfig;
import gusl.node.jersey.JerseyNodeClient;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

import static gusl.core.utils.IOUtils.waitForService;

/**
 * @author dhudson
 * @since 06/10/2021
 */
@Service
@CustomLog
public class KibanaClientImpl extends JerseyNodeClient implements KibanaClient {

    private KibanaConfig theConfig;

    public KibanaClientImpl() {
        super(JerseyHttpConfig.of(ObjectMapperFactory.getDefaultObjectMapper()));
    }

    @Override
    public void createIndexPattern(IndexPattern pattern) throws IOException {
        Invocation.Builder invocationBuilder = getJsonBuilder(getBaseUrl() + "/api/saved_objects/index-pattern/" + pattern.getAttributes().getTitle());
        preFlightInvocation(invocationBuilder);
        Response response = invocationBuilder.post(buildJsonEntity(pattern));
    }

    @Override
    public boolean isUp() {
        return getKibanaStats().isUp();
    }

    @Override
    public KibanaStats getKibanaStats() {
        Invocation.Builder invocationBuilder = getJsonBuilder(getBaseUrl() + "/api/status");
        preFlightInvocation(invocationBuilder);
        return invocationBuilder.get(KibanaStats.class);
    }

    @Override
    public void waitForStartup(Duration duration) throws TimeoutException {
        logger.info("Waiting for Kibana Connection to start {}", theConfig);
        waitForService(theConfig.getHost(), ElasticConstants.KIBANA_DEFAULT_PORT, duration, logger);

        while (true) {
            try {
                if (isUp()) {
                    return;
                }
            } catch (Throwable ignore) {
            }

            logger.info("Waiting for Kibana ...");
            Utils.sleep(2000);
        }
    }

    private String getBaseUrl() {
        return String.format("http://%s:%d", theConfig.getHost(), ElasticConstants.KIBANA_DEFAULT_PORT);
    }

    private void preFlightInvocation(Invocation.Builder invocationBuilder) {
        addBasicAuthorisation(invocationBuilder, encode(theConfig.getUsername(), theConfig.getPassword()));
        invocationBuilder.header("kbn-xsrf", "true");
    }

    @Override
    public void configure(KibanaConfig config) {
        theConfig = config;
    }

    public KibanaConfig getConfig() {
        return theConfig;
    }
}
