package gusl.kibana.client;

import gusl.elastic.kibana.IndexPattern;
import gusl.kibana.config.KibanaConfig;
import gusl.kibana.model.KibanaStats;
import org.jvnet.hk2.annotations.Contract;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

/**
 * @author dhudson
 * @since 06/10/2021
 */
@Contract
public interface KibanaClient {

    void configure(KibanaConfig config);

    boolean isUp();

    KibanaStats getKibanaStats();

    void createIndexPattern(IndexPattern pattern) throws IOException;

    void waitForStartup(Duration duration) throws TimeoutException;
}
